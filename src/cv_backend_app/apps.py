from django.apps import AppConfig


class CvBackendAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cv_backend_app'
