from django.db import models

# Create your models here.


class Experience(models.Model):
    title = models.CharField()
    # maybe have this somehow mapped to LinkedIn things?
    employer = models.CharField()
    location = models.CharField()
    startdate = models.DateField()
    enddate = models.DateField(null=True)
    description = models.TextField(max_length=2000)


class Proficiency(models.Model):
    name = models.CharField()
    level = models.IntegerField()  # somehow make sure that this is between 0 and 100 (really 0?)
