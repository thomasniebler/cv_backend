from django.shortcuts import render


# Create your views here.
def entries(request):
    entries_dict = {}
    return render(request, "entries.html", entries_dict)
